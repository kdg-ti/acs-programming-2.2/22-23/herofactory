import knight.BeanFactory;
import knight.Hero;
import knight.HeroFactory;

public class Runner {
  public static void main(String[] args) {
    BeanFactory context = new HeroFactory();
    Hero ladies = context.hero("ladies");
    ladies.embarkOnMission();
    Hero handy  = context.hero("handy");
    handy.embarkOnMission();
  }
}
