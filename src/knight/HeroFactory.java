package knight;

public class HeroFactory implements BeanFactory {

  //  factory method
  @Override
  public  Hero hero(String type){
    return switch(type){
      case "ladies" -> new LadiesKnight();
      default -> new HandyKnight(new Errand());
    };
  }
}
