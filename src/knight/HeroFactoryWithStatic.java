package knight;

public class HeroFactoryWithStatic {

  // static factory method
  public static Hero hero(String type){
    return switch(type){
      case "ladies" -> new LadiesKnight();
      default -> new HandyKnight(new Errand());
    };
  }
}
