package knight;

public class HandyKnight implements Hero {

  Mission mission;

  public HandyKnight(Mission mission) {
    this.mission = mission;
  }

  @Override
  public void embarkOnMission(){
       mission.embark();
  }
}
