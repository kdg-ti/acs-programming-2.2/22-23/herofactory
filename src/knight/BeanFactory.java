package knight;

public interface BeanFactory {
  //  factory method
  Hero hero(String type);
}
