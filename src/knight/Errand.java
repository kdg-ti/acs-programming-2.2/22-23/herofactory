package knight;

public class Errand implements Mission {
  @Override
  public void embark() {
    System.out.println("Quest: Burn this message after reading");
  }
}
